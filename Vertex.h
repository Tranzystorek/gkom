#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>

struct Color
{
	Color(GLfloat rr = 0.f, GLfloat gg = 0.f, GLfloat bb = 0.f) : r(rr), g(gg), b(bb) {}

	static Color fromRGB8(int r, int g, int b);

	GLfloat r;
	GLfloat g;
	GLfloat b;
};

class Vertex
{
public:
	Vertex();
	Vertex(GLfloat x, GLfloat y, GLfloat z, Color c = Color(), glm::vec3 n = glm::vec3(), glm::vec2 tex = glm::vec2());
	Vertex(GLfloat x, GLfloat y, GLfloat z, glm::vec3 n);

	Vertex clone() const;
	Vertex& rotateXY(GLfloat deg);
	Vertex& scaleXY(float ratio);
	Vertex& move(GLfloat dx, GLfloat dy, GLfloat dz);

	Vertex& setColor(Color c) { color_ = c; return *this; }
	Vertex& setNorm(glm::vec3 n) { norm_ = n; return *this;  }

	glm::vec3 getVec3() const;

	GLfloat x() const { return x_; }
	GLfloat y() const { return y_; }
	GLfloat z() const { return z_; }

	Color color() const { return color_; }

private:
	GLfloat x_;
	GLfloat y_;
	GLfloat z_;

	Color color_;
	glm::vec3 norm_;
	glm::vec2 textcord_;
};

