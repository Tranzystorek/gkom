#version 330 core
layout (location = 0) in vec3 pos;
layout (location = 1) in vec3 col;
layout (location = 2) in vec3 norm;
layout (location = 3) in vec2 texcord;

uniform mat4 camera;
uniform vec3 campos;
uniform mat4 model;

out vec3 fragPos;
out vec3 viewPos;
out vec3 lightPos;
out vec3 vertexCol;
out vec3 vnorm;
out vec2 vTexcord;

void main()
{
	gl_Position = camera * model * vec4(pos, 1.0);

	lightPos = vec3(0.0, 0.0, 180.0);
	
	fragPos = vec3(model * vec4(pos, 1.0));
	viewPos = campos;
	vertexCol = col;
	vnorm = mat3(model) * norm;
	vTexcord = texcord;
}