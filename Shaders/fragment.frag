#version 330 core

in vec3 fragPos;
in vec3 viewPos;
in vec3 lightPos;
in vec3 vertexCol;
in vec3 vnorm;
in vec2 vTexcord;

out vec4 color;

uniform bool textured;
uniform sampler2D text;

void main()
{
	vec3 lightdir = normalize(lightPos - fragPos);

	float ambientStrength = 0.2;
	vec3 lightColor = vec3(1.0, 1.0, 1.0);
	vec3 ambient = ambientStrength * lightColor;
	
	float diff = max(dot(vnorm, lightdir), 0.0);
	vec3 diffuse = diff * lightColor;

	float specularStrength = 0.4;
	vec3 viewdir = normalize(viewPos - fragPos);
	vec3 reflectdir = reflect(-lightdir, vnorm);

	float spec = pow(max(dot(viewdir, reflectdir), 0.0), 4);
	vec3 specular = specularStrength * spec * lightColor;

	//here the branching magic happens
	if(textured)
	{
		vec3 result = (ambient + diffuse + specular) * texture(text, vTexcord).rgb;
		color = vec4(result, 1.0);
	}
	else
	{
		vec3 result = (ambient + diffuse + specular) * vertexCol;
		color = vec4(result, 1.0);
	}
}