#include "Camera.h"

#include <iostream>

#include <glm/gtc/matrix_transform.hpp>

Camera::Camera(glm::vec3 pos, glm::vec3 look_at, GLfloat fov, GLfloat ratio, GLfloat near, GLfloat far)
	: proj_(glm::perspective(glm::radians(fov), ratio, near, far)),
	  pos_(pos),
	  look_(look_at - pos_)
{
	dir_ = glm::normalize(look_);
	forw_ = glm::normalize(glm::vec3(look_.x, look_.y, 0.f));
	
	right_ = glm::normalize(glm::cross(dir_, glm::vec3(0.f, 0.f, 1.f)));
	up_ = glm::normalize(glm::cross(right_, dir_));

	view_ = glm::lookAt(pos_, look_at, up_);
}

glm::mat4 Camera::getMatrix() const
{
	return proj_ * view_;
}

glm::vec3 Camera::getDir() const
{
	return dir_;
}

void Camera::strafe(GLfloat ds)
{
	pos_ += right_ * ds;

	//view_ = glm::lookAt(pos_, pos_ + look_, up_);
}

void Camera::pedestal(GLfloat ds)
{
	pos_ += glm::vec3(0.f, 0.f, 1.f) * ds;

	//view_ += glm::lookAt(pos_, pos_ + look_, up_);
}

void Camera::toward(GLfloat ds)
{
	pos_ += dir_ * ds;

	//view_ = glm::lookAt(pos_, pos_ + look_, up_);
}

void Camera::forward(GLfloat ds)
{

	pos_ += forw_ * ds;

	//view_ = glm::lookAt(pos_, pos_ + look_, up_);
}

void Camera::orbit(GLfloat dang)
{
	glm::mat3 rot = glm::mat3(glm::rotate(glm::mat4(), glm::radians(dang), glm::vec3(0.f, 0.f, 1.f)));

	pos_ = rot * pos_;
	look_ = rot * look_;

	dir_ = rot * dir_;
	up_ = rot * up_;
	right_ = rot * right_;
	forw_ = rot * forw_;

	//view_ = glm::lookAt(pos_, pos_ + look_, up_);
}

void Camera::update()
{
	view_ = glm::lookAt(pos_, pos_ + look_, up_);
}
