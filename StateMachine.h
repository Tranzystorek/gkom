#pragma once

class StateMachine
{
public:
	enum State {FORWARD, BACKWARD};

public:
	StateMachine(double period, State initial = State::BACKWARD);

	void update(double dt);
	State getState() const { return state_; }

private:
	State state_;

	double t_;
	double period_;
};

