#include "ObjectManager.h"

#include <glm/gtc/type_ptr.hpp>
#include <SOIL/SOIL.h>

ObjectManager::ObjectManager()
{

}

ObjectManager::~ObjectManager()
{
	for (auto& obj : objects_)
	{
		glDeleteVertexArrays(1, &(obj.vao));

		if (obj.textflag == Box::TEXTURED)
			glDeleteTextures(1, &(obj.texture));
	}
}

void ObjectManager::addObject(Object* obj, GLenum usage, Box::Textured tx, const std::string& texpath)
{
	GLuint vao, vbo, ebo;
	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);
	glGenBuffers(1, &ebo);

	glBindVertexArray(vao);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, 11 * obj->getNVerts() * sizeof(GLfloat), obj->getData(), usage);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, obj->getNIndices() * sizeof(GLuint), obj->getIndices(), usage);

	//coordinates
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void*)0);
	glEnableVertexAttribArray(0);
	
	//color
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);

	//normal vector
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void*)(6 * sizeof(GLfloat)));
	glEnableVertexAttribArray(2);

	//texture coordinates
	glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void*)(9 * sizeof(GLfloat)));
	glEnableVertexAttribArray(3);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glDeleteBuffers(1, &vbo);
	glDeleteBuffers(1, &ebo);

	//add texture if present
	if (tx == Box::TEXTURED)
	{
		int width, height;
		unsigned char* img = SOIL_load_image(texpath.c_str(), &width, &height, 0, SOIL_LOAD_RGB);

		GLuint tex;
		glGenTextures(1, &tex);

		glBindTexture(GL_TEXTURE_2D, tex);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, img);
		glGenerateMipmap(GL_TEXTURE_2D);

		SOIL_free_image_data(img);
		glBindTexture(GL_TEXTURE_2D, 0);

		objects_.emplace_back(obj, vao, tx, tex);
	}

	//add object to vector
	else
		objects_.emplace_back(obj, vao, tx);
}

void ObjectManager::draw(Camera& camera, Shader& sh)
{
	GLuint camLoc = glGetUniformLocation(sh.id(), "camera");
	GLuint camposLoc = glGetUniformLocation(sh.id(), "campos");
	GLuint modelLoc = glGetUniformLocation(sh.id(), "model");
	GLuint texturedLoc = glGetUniformLocation(sh.id(), "textured");
	GLuint samplerLoc = glGetUniformLocation(sh.id(), "text");

	glm::vec3 campos = camera.getPos();

	glUniformMatrix4fv(camLoc, 1, GL_FALSE, glm::value_ptr(camera.getMatrix()));
	glUniform3fv(camposLoc, 1, glm::value_ptr(campos));

	for (auto& obj : objects_)
	{
		glm::mat4 model = obj->getModel();
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

		glUniform1i(texturedLoc, obj.textflag == Box::TEXTURED);

		sh.use_program();

		if (obj.textflag == Box::TEXTURED)
		{
			glUniform1i(samplerLoc, 0);

			glBindTexture(GL_TEXTURE_2D, obj.texture);
			glBindVertexArray(obj.vao);
			glDrawElements(GL_TRIANGLES, obj->getNIndices(), GL_UNSIGNED_INT, 0);
			glBindTexture(GL_TEXTURE_2D, 0);
		}

		else
		{
			glBindVertexArray(obj.vao);
			glDrawElements(GL_TRIANGLES, obj->getNIndices(), GL_UNSIGNED_INT, 0);
		}
	}

	glBindVertexArray(0);
}
