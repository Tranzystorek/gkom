#pragma once

class Keys
{
public:
	Keys();

	void update(int key, int action);

	bool w() const { return w_; }
	bool s() const { return s_; }
	bool a() const { return a_; }
	bool d() const { return d_; }
	bool q() const { return q_; }
	bool e() const { return e_; }
	bool r() const { return r_; }
	bool f() const { return f_; }
	bool up() const { return up_; }
	bool down() const { return down_; }

private:
	bool w_;
	bool s_;
	bool a_;
	bool d_;
	bool q_;
	bool e_;
	bool r_;
	bool f_;
	bool up_;
	bool down_;
};

