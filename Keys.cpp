#include "Keys.h"

#include <GLFW/glfw3.h>
#include <iostream>

Keys::Keys()
	: w_(false),
	  s_(false),
	  a_(false),
	  d_(false),
	  q_(false),
	  e_(false),
	  r_(false),
	  f_(false)
{
}

void Keys::update(int key, int action)
{
	switch (key)
	{
	case GLFW_KEY_W:
		w_ = (action != GLFW_RELEASE);
		break;
	case GLFW_KEY_S:
		s_ = (action != GLFW_RELEASE);
		break;
	case GLFW_KEY_A:
		a_ = (action != GLFW_RELEASE);
		break;
	case GLFW_KEY_D:
		d_ = (action != GLFW_RELEASE);
		break;
	case GLFW_KEY_Q:
		q_ = (action != GLFW_RELEASE);
		break;
	case GLFW_KEY_E:
		e_ = (action != GLFW_RELEASE);
		break;
	case GLFW_KEY_R:
		r_ = (action != GLFW_RELEASE);
		break;
	case GLFW_KEY_F:
		f_ = (action != GLFW_RELEASE);
		break;
	case GLFW_KEY_UP:
		up_ = (action != GLFW_RELEASE);
		break;
	case GLFW_KEY_DOWN:
		down_ = (action != GLFW_RELEASE);
		break;

	default:
		break;
	}
}
