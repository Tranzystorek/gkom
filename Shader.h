#pragma once

#include <GL/glew.h>

class Shader
{
public:
	Shader(const GLchar* vertpath, const GLchar* fragpath);
	~Shader();

	GLuint id() const;
	void use_program();

private:
	GLuint program_id_;
};

