#pragma once

#include <GL/glew.h>

#include <memory>
#include <vector>

#include "Object.h"
#include "Camera.h"
#include "Shader.h"

struct Box
{
	enum Textured {TEXTURED, UNTEXTURED};

	Box(Object* obj, GLuint varray, Textured txf = Textured::UNTEXTURED, GLuint textid = 0)
		: ptr(obj),
		  vao(varray),
		  textflag(txf),
		  texture(textid)
	{}

	Object* operator->() { return ptr.get(); }

	std::unique_ptr<Object> ptr;
	GLuint vao;

	Textured textflag;
	GLuint texture;
};

class ObjectManager
{
public:
	ObjectManager();
	~ObjectManager();

	void addObject(Object* obj, GLenum usage, Box::Textured tx = Box::UNTEXTURED, const std::string& texpath = "");
	void draw(Camera& camera, Shader& sh);

private:
	std::vector<Box> objects_;
};

