#include "Animation.h"

#define PI 3.14159265f

Animation::Animation(GLfloat cogr, GLfloat cogw, GLfloat cogn, GLfloat logr, GLfloat loglen, GLfloat screenratio, double swtime)
	: sh_("Shaders/vertex.vert", "Shaders/fragment.frag"),
	  camera_(glm::vec3(0.f, -600.f, 400.f), glm::vec3(0.f, 0.f, logr), 45.f, screenratio, 0.1f, 10000.f),
	  sm_(swtime),
	  switch_time_(swtime),
	  log_speed_(loglen / swtime),
	  rot_speed_((180.f / (PI * cogr) * log_speed_)),
	  move_(Move::M_NONE),
	  walk_(Walk::W_NONE),
	  strafe_(Strafe::S_NONE),
	  orbit_(Orbit::O_NONE),
	  pedestal_(Pedestal::P_NONE),
	  left_(new CogWheel(cogr, 0.15f * cogr, cogw, cogn, Color::fromRGB8(112, 128, 144), CogWheel::COGS)),
	  right_(new CogWheel(*left_)),
	  left_ax_(new Cylinder(0.15f * cogr, cogw + 40.f, 30, Color::fromRGB8(84, 84, 84))),
	  right_ax_(new Cylinder(*left_ax_)),
	  log_(new Cylinder(logr, loglen, 60, Color::fromRGB8(133, 94, 66))),
	  pad_(new Cuboid(4*cogr + 2*logr + 20.f, 2*loglen + 20.f, 20.f, Color::fromRGB8(142, 107, 35), Cuboid::OUTWARDS)),
	  room_(new Cuboid(3 * (4 * cogr + 2 * logr + 20.f), 1.5f*(2 * loglen + 20.f), 12 * logr, Color::fromRGB8(74, 118, 110)))
{
	//setup objects
	left_->move(-cogr - logr, 0.f, 30.f);
	left_ax_->move(-cogr - logr, 0.f, 0.f);

	right_->move(cogr + logr, 0.f, 30.f);
	right_ax_->move(cogr + logr, 0.f, 0.f);

	log_->rotate(90.f, glm::vec3(-1.f, 0.f, 0.f));
	log_->setPos(0.f, 0.f, logr + 20.f);

	pad_->move(-2*cogr - logr - 10.f, -loglen - 10.f, 0.f);

	room_->move(-room_->getW() / 2.f, -room_->getH() / 2.f, 0.f);

	//add to manager
	manager_.addObject(log_, GL_DYNAMIC_DRAW);
	
	manager_.addObject(left_, GL_DYNAMIC_DRAW);
	manager_.addObject(right_, GL_DYNAMIC_DRAW);

	manager_.addObject(left_ax_, GL_STATIC_DRAW);
	manager_.addObject(right_ax_, GL_STATIC_DRAW);

	manager_.addObject(pad_, GL_STATIC_DRAW);
	//manager_.addObject(room_, GL_STATIC_DRAW);
	manager_.addObject(room_, GL_STATIC_DRAW, Box::TEXTURED, std::string("walls_texture.png"));
}

void Animation::start()
{
	timer_.start();
	sm_ = StateMachine(switch_time_);
}

void Animation::update()
{
	double dt = timer_.restart();

	sm_.update(dt);

	//walk
	switch (walk_)
	{
	case Walk::FORWARD:
		camera_.forward(700.f * dt);
		break;
	case Walk::BACKWARD:
		camera_.forward(-700.f * dt);
		break;
	default:
		break;
	}

	//move
	switch (move_)
	{
	case Move::TOWARD:
		camera_.toward(400.f * dt);
		break;
	case Move::FROM:
		camera_.toward(-400.f * dt);
		break;
	default:
		break;
	}

	//strafe
	switch (strafe_)
	{
	case Strafe::S_LEFT:
		camera_.strafe(-300.f * dt);
		break;
	case Strafe::S_RIGHT:
		camera_.strafe(300.f * dt);
		break;
	default:
		break;
	}

	//orbit
	switch (orbit_)
	{
	case Orbit::O_LEFT:
		camera_.orbit(-100.f * dt);
		break;
	case Orbit::O_RIGHT:
		camera_.orbit(100.f * dt);
		break;
	default:
		break;
	}

	//pedestal
	switch (pedestal_)
	{
	case Pedestal::UP:
		camera_.pedestal(300.f * dt);
		break;
	case Pedestal::DOWN:
		camera_.pedestal(-300.f * dt);
		break;
	default:
		break;
	}

	//update camera
	camera_.update();

	//animate
	switch (sm_.getState())
	{
	case StateMachine::BACKWARD:
		log_->move(0.f, -log_speed_ * dt, 0.f);
		left_->rotate(-rot_speed_ * dt);
		right_->rotate(rot_speed_ * dt);
		break;

	case StateMachine::FORWARD:
		log_->move(0.f, log_speed_ * dt, 0.f);
		left_->rotate(rot_speed_ * dt);
		right_->rotate(-rot_speed_ * dt);
		break;
	}
}

void Animation::render()
{
	manager_.draw(camera_, sh_);
}

void Animation::process_input(const Keys& keys)
{
	if (keys.w() && !keys.s())
		walk_ = Walk::FORWARD;
	else if (keys.s() && !keys.w())
		walk_ = Walk::BACKWARD;
	else
		walk_ = Walk::W_NONE;

	if (keys.up() && !keys.down())
		move_ = Move::TOWARD;
	else if (keys.down() && !keys.up())
		move_ = Move::FROM;
	else
		move_ = Move::M_NONE;

	if (keys.a() && !keys.d())
		strafe_ = Strafe::S_LEFT;
	else if (keys.d() && !keys.a())
		strafe_ = Strafe::S_RIGHT;
	else
		strafe_ = Strafe::S_NONE;

	if (keys.q() && !keys.e())
		orbit_ = Orbit::O_LEFT;
	else if (keys.e() && !keys.q())
		orbit_ = Orbit::O_RIGHT;
	else
		orbit_ = Orbit::O_NONE;

	if (keys.r() && !keys.f())
		pedestal_ = Pedestal::UP;
	else if (keys.f() && !keys.r())
		pedestal_ = Pedestal::DOWN;
	else
		pedestal_ = Pedestal::P_NONE;
}
