#pragma once
#include "Object.h"

class CogWheel : public Object
{
public:
	enum Shape
	{
		STAR,
		SPIKES,
		COGS,
	};

public:
	CogWheel(GLfloat r, GLfloat d, GLfloat w, GLuint n, Color c, Shape shape = STAR);

	void rotate(GLfloat deg);

private:
	void gen_star();
	void gen_spikes();
	void gen_cogs();

private:
	GLfloat radius_;
	GLfloat d_;
	GLfloat width_;
	GLuint ncogs_;
	Color color_;
	Shape shape_;

	GLfloat rotation_;
};

