#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <stdexcept>

#include "Shader.h"

std::string load_shader_from_file(const GLchar* path)
{
	std::ifstream shdrfile;
	std::stringstream shdrstream;
	
	shdrfile.exceptions(std::ifstream::badbit);

	shdrfile.open(path);
	shdrstream << shdrfile.rdbuf();
	shdrfile.close();

	return shdrstream.str();
}

GLuint compile_shader(const GLchar* shdrcode, GLenum shdrtype)
{
	GLuint shdr_id = glCreateShader(shdrtype);
	glShaderSource(shdr_id, 1, &shdrcode, NULL);
	glCompileShader(shdr_id);

	GLint success;
	glGetShaderiv(shdr_id, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		GLchar infolog[512];
		glGetShaderInfoLog(shdr_id, sizeof(infolog), NULL, infolog);
		std::string msg = std::string("Shader compilation: ") + infolog;
		throw std::runtime_error(msg.c_str());
	}

	return shdr_id;
}

Shader::Shader(const GLchar* vertpath, const GLchar* fragpath)
{
	std::string vert = load_shader_from_file(vertpath);
	GLuint vert_id = compile_shader(vert.c_str(), GL_VERTEX_SHADER);

	std::string frag = load_shader_from_file(fragpath);
	GLuint frag_id = compile_shader(frag.c_str(), GL_FRAGMENT_SHADER);

	program_id_ = glCreateProgram();
	glAttachShader(program_id_, vert_id);
	glAttachShader(program_id_, frag_id);
	glLinkProgram(program_id_);

	GLint success;
	glGetProgramiv(program_id_, GL_LINK_STATUS, &success);
	if (!success)
	{
		GLchar infolog[512];
		glGetProgramInfoLog(program_id_, sizeof(infolog), NULL, infolog);
		std::string msg = std::string("Shader program linking: ") + infolog;
		throw std::runtime_error(msg.c_str());
	}

	glDeleteShader(frag_id);
	glDeleteShader(vert_id);
}

Shader::~Shader()
{
	glDeleteProgram(program_id_);
}

GLuint Shader::id() const
{
	return program_id_;
}

void Shader::use_program()
{
	glUseProgram(program_id_);
}

