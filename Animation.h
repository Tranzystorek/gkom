#pragma once

#include "Shader.h"
#include "Timer.h"
#include "StateMachine.h"
#include "ObjectManager.h"
#include "Camera.h"

#include "CogWheel.h"
#include "Cylinder.h"
#include "Cuboid.h"

#include <GLFW/glfw3.h>
#include "Keys.h"

enum Strafe
{
	S_NONE,
	S_LEFT,
	S_RIGHT
};

enum Pedestal
{
	P_NONE,
	UP,
	DOWN
};

enum Orbit
{
	O_NONE,
	O_LEFT,
	O_RIGHT
};

enum Move
{
	M_NONE,
	TOWARD,
	FROM
};

enum Walk
{
	W_NONE,
	FORWARD,
	BACKWARD
};

class Animation
{
public:
	Animation(GLfloat cogr, GLfloat cogw, GLfloat cogn,
		      GLfloat logr, GLfloat loglen,
		      GLfloat screenratio, double swtime);

	void start();
	void update();
	void render();
	void process_input(const Keys& keys);

private:
	Shader sh_;
	ObjectManager manager_;
	Camera camera_;
	Timer timer_;
	StateMachine sm_;

private:
	double switch_time_;
	GLfloat log_speed_;
	GLfloat rot_speed_;

	Move move_;
	Walk walk_;
	Strafe strafe_;
	Orbit orbit_;
	Pedestal pedestal_;

private:
	CogWheel* left_;
	CogWheel* right_;

	Cylinder* left_ax_;
	Cylinder* right_ax_;

	Cylinder* log_;
	
	Cuboid* pad_;
	Cuboid* room_;
};

